Getting Things Done
===================

Files:
  - newgtd.org
  - reference.org
  - refile.org

newgtd.org
----------

File Category: PERSONAL

Headers:
  * Tasks
     Any actionable item that is planned to do.
     Tags:
        (Based on Eisenhower matrix)
        - @urgent
        - @important
        - @noturgent
        - @notimportant
        (Everthing else that doesn't fit into above categories, go into below tag)
        - someday/maybe

        (Also additional tags)
        - read
        - write
        - watch
        - attend

  * Purchases
      Items that planned to buy.
      Tags:
        - grocery        (Applies to any food item)
        - equipment      (Applies to any gadget/system item)
        - home           (Applies to appliances at home)
        - me             (Applies to items for myself)

  * Projects
     Classify items by projects inside this header.
     PROJECT tag to the header.

  * Calendar
     Remainders only. For the notification purpose only. Add events
     that are scheduled into Tasks with attend tag.


reference.org
-------------

File Category: REFERENCE

Headers:
  * Notes
     All the digital notes. Add created date for sorting according
     to the dates. Also divide notes with book header.

  * Social Media Posts

  * Quotes

  * Books
    ** Computer Science
       Tags:
         - software
         - architect
         - programming
         - algorithms
         - python
         - golang (etc.)

    ** Fiction
    ** Non fiction
       Tags:
         - money
         - biography
         - technical
         - self_help
         - productivity (etc.)

refile.org
----------

File Category: REFILE

Headers:
  * Captured Stuff
     Tasks or references that are yet to classify. Items which are
     incubate state. Captured items first put here if not directly
     classified. Org capture from Browser should add links here and
     then supply to either `reference.org` or `newgtd.org`
     files. Process before refiling these items.
