Review
======

Files:
  - weekly_review.org
  - monthly_review.org

weekly_review.org
-----------------

  "If you're not doing the weekly review, you're not doing GTD!"
   Ricky's RAM Dump: http://www.rickyspears.com/blog

  "It's called a weekly review, not a weakly review!"

  "Seven days without a review makes one weak!"

Notes from David Allen's book "Ready for Anything".

The first challenge is to implement these models, and the second is to
keep them active and functional. This guide provides the master key to
achieving a consistently more relaxed and productive style of life and
work. This process, whenever it's done, facilitates executive
command-center thinking and confidence, and it's most effective when
it's practiced every seven days.


monthly_review.org
------------------

Review your short term goals monthly to keep train on track.
