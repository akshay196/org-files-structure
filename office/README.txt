Office
======

Tasks, plans related to office are stored in this directory.

Structure:
  - office_work.org
  - plan
  - sprints

office_work.org
---------------

File Category: OFFICE

Todo tasks of office.

Headers:
  * Tasks
     Actionable office tasks that are planned to do.

  * Calendar
     Office meetings as reminders.

  * Review
     A datetree for the monthly review of the office work.

     Example of datetree:
     * 2020
     ** 2020-06 June

  * References
    References links related to the office work.
