Goals
=====

Files:
  - goals.org

goals.org
---------

Add goals as top level header in the file. Inside a goal add
plans. Add notes into goal and plan that thoroughly describe it
indetail. You may include action items into plan header. But do not
duplicate items in the gtd directory. Remember to remove action item
when it is moved to `gtd` directory.

Add deadlines to the goals and review short term goals monthly.

For example:
  * Reader
    * Read a single post per day.
    * Gather books
  * Public Speaker
    * Go to stage
      - [ ] Read about stage daring
